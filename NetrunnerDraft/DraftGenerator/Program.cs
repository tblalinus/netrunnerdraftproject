﻿//To do: refactorisering

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DraftGenerator
{
    class Program
    {
        public Random rnd = new Random();
        public List<Card> cardList = new List<Card>();
        public List<List<Card>> hands = new List<List<Card>>();
        public List<List<Card>> playerCards = new List<List<Card>>();
        public List<List<Card>> playerDecks = new List<List<Card>>();
        

        static void Main(string[] args)
        {
            Program program = new Program();
            
            program.ListGenerator();
            program.Menu();
        }

        public void ListGenerator()
        {
            for (int i = 0; i < 100; i++)
            {
                var card = new Card();
                card.name = $"card{i + 1}";
                cardList.Add(card);
            }
        }

        public void Menu()
        {
            var menuRun = true;
            while(menuRun == true)
            {
                Console.WriteLine("N: New Game    L: Display List of cards    Q: Quit");
                var selection = char.ToUpper(Console.ReadKey().KeyChar);
                switch (selection)
                {
                    case 'N':
                        Console.Clear();
                        hands.Clear();
                        playerCards.Clear();
                        DraftControl();
                        break;
                    case 'L':
                        PrintCards(cardList);
                        break;
                    case 'Q':
                        menuRun = false;
                        break;
                    default:
                        Console.WriteLine("Unknown Command");
                        break;
                }
            }
        }

        private void DraftControl()
        {
            int numberOfPlayers;
            int handSize;
            int numberOfHands;
            GetDraftParameters(out handSize, out numberOfHands);
            GetNumberOfPlayers(out numberOfPlayers);
            for (int i = 0; i< numberOfPlayers; i++)
            {
                hands.Add(new List<Card>());
                playerCards.Add(new List<Card>());
                playerDecks.Add(new List<Card>());
            }
            Drafting(handSize, numberOfHands, numberOfPlayers);
            for (int i =0; i<numberOfPlayers; i++)
            {
                DisplayDeck(i);
            }
        }

        private void GetDraftParameters(out int handSize, out int numberOfHands)
        {
            Console.Clear();
            Console.WriteLine("Enter number of hands");
            var numberLine = Console.ReadLine();
            while (!int.TryParse(numberLine, out numberOfHands))
            {
                Console.Write("Please enter a numeric value: ");
                numberLine = Console.ReadLine();
            }

            Console.WriteLine();
            Console.WriteLine("Enter hand size");
            var sizeLine = Console.ReadLine();
            while (!int.TryParse(sizeLine, out handSize))
            {
                Console.Write("Please enter a numeric value: ");
                sizeLine = Console.ReadLine();
            }
        }

        private void GetNumberOfPlayers(out int numberOfPlayers)
        {
            Console.Clear();
            Console.WriteLine("Enter number of players");
            
            var line = Console.ReadLine();
            while (!int.TryParse(line, out numberOfPlayers))
            {
                Console.Write("Please enter a numeric value: ");
                line = Console.ReadLine();
            }
        }

        private void Drafting(int handSize, int numberOfHands, int numberOfPlayers)
        {
            for (int i = 0; i < numberOfHands; i++)
            {    
                Setup(numberOfPlayers, handSize);
                int j = 0;
                int handNumber;
                while (hands[0].Count > 0)
                {
                    for( int player=0 ; player<numberOfPlayers; player++)
                    {
                        if (player + j >= 2)
                            handNumber = player + j - 2;
                        else
                            handNumber = player + j;
                        PlayerTurn(player, handNumber);
                    }
                    if ( j == 1)
                        j = 0;
                    else
                        j ++;
                }
            }
        }

        public void DisplayDeck(int player)
        {
            Console.WriteLine($"Player{player+1}'s Deck press any key:");
            Console.ReadKey();
            PrintCards(playerDecks[player]);
            Console.ReadKey();
            Console.Clear();                  
        }

        public void PrintCards(List<Card> _cardList)
        {
            foreach(Card card in _cardList)
            {
                Console.WriteLine(card.name);
            }
        }









        public void Setup(int numberOfPlayers, int handSize)
        {
            Card usedCard;
            for (int i = 0; i < numberOfPlayers; i++)
            {
                hands[i].Clear();
                playerCards[i].Clear();
                for (int j = 0; j < handSize; j++)
                {
                    usedCard = Randomizer();
                    hands[i].Add(usedCard);
                    cardList.Remove(usedCard);
                }
            }
        }

        public Card Randomizer()
        {
                var Randomcard = cardList[rnd.Next(0, cardList.Count)];
                cardList.Remove(Randomcard);
                return Randomcard; 
        }

        public void PlayerTurn(int player, int hand)
        {
            Console.WriteLine($"Player {player+1}'s turn. Press any key");
            Console.ReadKey();
            Console.Clear();
            DisplayAvailable(hand);
            DisplayPicked(player);
            CardPicker(player, hand);
        }

        public void DisplayAvailable(int hand)
        {
            Console.WriteLine("Avaiable cards:");
            foreach (Card card in hands[hand])
            {
                Console.WriteLine($"{(hands[hand].IndexOf(card) + 1).ToString("D2")}  {card.name}");
            }
            Console.WriteLine();
        }

        public void DisplayPicked(int player)
        {
            Console.WriteLine("Picked cards:");
            foreach (Card card in playerCards[player])
            {
                Console.WriteLine(card.name);
            }
            Console.WriteLine();
        }

        public void CardPicker(int player, int hand)
        {
            Console.WriteLine("Pick a card 1-10");
            int entry;
            bool pick = false;
            while (pick == false)
            {
                pick = int.TryParse(Console.ReadLine(), out entry);
                if (pick == false)
                {
                    Console.WriteLine("Invalid entry");
                }
                else if (entry > hands[hand].Count)
                {
                    Console.WriteLine("Card does not exist");
                    pick = false;
                }
                else
                {
                    var pickedCard = hands[hand][entry - 1];
                    playerCards[player].Add(pickedCard);
                    playerDecks[player].Add(pickedCard);
                    hands[hand].Remove(pickedCard);
                    Console.Clear();
                }
            }
        }



    }

}
