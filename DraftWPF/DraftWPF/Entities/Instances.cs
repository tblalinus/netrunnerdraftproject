﻿using DraftWPF.Pages;

namespace DraftWPF.Entities
{
    public static class Instances
    {
        public static MainWindow mainWindow { get; set; }

        public static MainMenuPage mainMenuPage { get; set; }
        public static GamePage gamePage { get; set; }

        public static CardPool cardPool { get; set; }
        public static Decks decks { get; set; }
    }
}
