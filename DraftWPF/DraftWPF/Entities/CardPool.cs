﻿using System.Collections.Generic;
using System.Linq;
using DraftWPF.Services;

namespace DraftWPF.Entities
{
    public class CardPool
    {
        public List<Card> allCards { get; }
        public List<Card> runnerPool { get; set; }
        public List<Card> corpPool { get; set; }

        public CardPool()
        {
            allCards = CreateCards.createCards();
        }

        public void CreateDraftPool()
        {
            runnerPool = CreateSidePool(Side.Runner);
            corpPool = CreateSidePool(Side.Corp);
        }

        private List<Card> CreateSidePool(Side side)
        {
            List<Card> sidePool = new List<Card>();
            List<Card> allCardsOfSide = new List<Card>(allCards.Where(c => c.CardSide == side));

            foreach (Card card in allCardsOfSide)
            {
                for (int i = 0; i < card.NumbersUsed; i++)
                {
                    sidePool.Add(new Card(card.CardSide, card.CardSet, card.CardNr));
                }
            }
            return sidePool;
        }
    }
}
