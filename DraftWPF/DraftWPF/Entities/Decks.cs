﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DraftWPF.Entities
{
    public class Decks
    {
        private int _numberOfPlayers;
        private Side _side;
        private List<Set> _sets;
        private List<Card> _cardPool;
        public ObservableCollection<Card>[] PlayerDecks { get; }
        public ObservableCollection<Card>[] Hands { get; set; }
        public ObservableCollection<Card>[] PlayerSelections { get; set; }

        public Decks(int numberOfPlayers, Side side, List<Set> sets)
        {
            _numberOfPlayers = numberOfPlayers;
            _side = side;
            _sets = sets;

            PlayerDecks = new ObservableCollection<Card>[_numberOfPlayers];
            for (int i = 0; i < _numberOfPlayers; i++)
                PlayerDecks[i] = new ObservableCollection<Card>();

            CreateDecks();
        }

        public void CreateDecks()
        {
            Hands = new ObservableCollection<Card>[_numberOfPlayers];
            PlayerSelections = new ObservableCollection<Card>[_numberOfPlayers];

            CheckSide();
            _cardPool = _cardPool.Where(c => _sets.Contains(c.CardSet)).ToList();
            FillDecks();
        }

        private void CheckSide()
        {
            if (_side == Side.Runner)
                _cardPool = Instances.cardPool.runnerPool;
            if (_side == Side.Corp)
                _cardPool = Instances.cardPool.corpPool;
        }

        private void FillDecks()
        {
            Random Rnd = new Random();

            for (int i = 0; i < _numberOfPlayers; i++)
            {
                Hands[i] = new ObservableCollection<Card>();
                PlayerSelections[i] = new ObservableCollection<Card>();

                for (int j = 0; j < 10; j++)
                {
                    Card card = _cardPool[Rnd.Next(0, _cardPool.Count)];
                    Hands[i].Add(card);
                    _cardPool.Remove(card);
                }
            }
        }

        public void SelectCard(int activePlayer, int handNumber, Card selectedCard)
        {
            Hands[handNumber-1].Remove(selectedCard);
            PlayerSelections[activePlayer-1].Add(selectedCard);
            PlayerDecks[activePlayer-1].Add(selectedCard);
        }
    }
}
