﻿
namespace DraftWPF.Entities
{
    public class Card
    {
        public Side CardSide { get; }
        public Set CardSet { get; }
        public int CardNr { get; }
        public int MaxNumbersUsed { get; }
        public int NumbersUsed { get; set; }

        public Card() { }

        public Card(Side side, Set cardSet, int cardNr, int maxNumbersUsed)
        {
            CardSide = side;
            CardSet = cardSet;
            CardNr = cardNr;
            MaxNumbersUsed = maxNumbersUsed;
            NumbersUsed = 3;
        }
    }
}
