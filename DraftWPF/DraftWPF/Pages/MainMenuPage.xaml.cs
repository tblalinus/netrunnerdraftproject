﻿using DraftWPF.Entities;
using System.Windows;
using System.Windows.Controls;

namespace DraftWPF.Pages
{
    public partial class MainMenuPage : Page
    {
        public MainMenuPage()
        {
            InitializeComponent();
            Instances.cardPool = new CardPool();
        }

        private void NewGame_OnClick(object sender, RoutedEventArgs e)
        {
            Instances.cardPool.CreateDraftPool();
            Instances.gamePage = new GamePage();
            Instances.mainWindow.MainWindowFrame.Content = Instances.gamePage;
        }

        private void CardList_OnClick(object sender, RoutedEventArgs e)
        {
            Instances.mainWindow.MainWindowFrame.Content = new CardListMenuPage();
        }

        private void Exit_OnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
