﻿using DraftWPF.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace DraftWPF.Pages
{
    public partial class GameMenuPage : Page
    {
        private int? numberOfPlayers = null;
        private Side? side = null;
        private List<Set> _setsSelected;

        public GameMenuPage()
        {
            InitializeComponent();
            _setsSelected = new List<Set>();
        }

        private void TwoButton_Checked(object sender, RoutedEventArgs e)
        {
            numberOfPlayers = 2;
        }

        private void ThreeButton_Checked(object sender, RoutedEventArgs e)
        {
            numberOfPlayers = 3;
        }

        private void FourButton_Checked(object sender, RoutedEventArgs e)
        {
            numberOfPlayers = 4;
        }

        private void FiveButton_Checked(object sender, RoutedEventArgs e)
        {
            numberOfPlayers = 5;
        }

        private void SixButton_Checked(object sender, RoutedEventArgs e)
        {
            numberOfPlayers = 6;
        }

        private void RunnerButton_Checked(object sender, RoutedEventArgs e)
        {
            side = Side.Runner;
        }

        private void CorpButton_Checked(object sender, RoutedEventArgs e)
        {
            side = Side.Corp;
        }

        private void CoreBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Core);
        }

        private void CoreBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Core);
        }

        private void GenesisBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Genesis);
        }

        private void GenesisBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Genesis);
        }

        private void CaCBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.CaC);
        }

        private void CaCBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.CaC);
        }

        private void SpinBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Spin);
        }

        private void SpinBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Spin);
        }

        private void HaPBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.HaP);
        }

        private void HaPBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.HaP);
        }

        private void LunarBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Lunar);
        }

        private void LunarBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Lunar);
        }

        private void OaCBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.OaC);
        }

        private void OaCBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.OaC);
        }

        private void SanSanBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.SanSan);
        }

        private void SanSanBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.SanSan);
        }

        private void DaDBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.DaD);
        }

        private void DaDBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.DaD);
        }

        private void MumbadBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Mumbad);
        }

        private void MumbadBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Mumbad);
        }

        private void FlashpointBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Flashpoint);
        }

        private void FlashpointBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Flashpoint);
        }

        private void TDBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.TD);
        }

        private void TDBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.TD);
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            int numberOfRunnerCards = Instances.cardPool.runnerPool.Where(c => _setsSelected.Contains(c.CardSet)).Count();
            int numberOfCorpCards = Instances.cardPool.corpPool.Where(c => _setsSelected.Contains(c.CardSet)).Count();

            if (numberOfPlayers.HasValue && side.HasValue)
            {
                int cardsRequired = (int)numberOfPlayers * 40;
                if (numberOfRunnerCards >= cardsRequired && numberOfCorpCards >= cardsRequired)
                {
                    Instances.decks = new Decks((int)numberOfPlayers, (Side)side, _setsSelected);
                    Instances.gamePage.GameFrame.Content = new DraftingPage((int)numberOfPlayers);
                }
                else
                    MessageBox.Show("Not enough cards");
            }
        }
    }
}
