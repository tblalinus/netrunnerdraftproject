﻿using DraftWPF.Entities;
using System.Windows;
using System.Windows.Controls;

namespace DraftWPF.Pages
{
    public partial class ResultPage : Page
    {
        private int _numberOfPlayers;
        private int _player;
        private Decks _decks = Instances.decks;

        public ResultPage(int numberOfPlayers)
        {
            InitializeComponent();
            _numberOfPlayers = numberOfPlayers;
            _player = 1;
            DisplayReadyPanel();
        }

        private void DisplayReadyPanel()
        {
            ResultGrid.Visibility = Visibility.Hidden;
            ReadyPanel.Visibility = Visibility.Visible;
            PlayerResultText.DataContext = $"PLAYER {_player}'s CARDS";
        }

        private void DisplayResultGrid()
        {
            ReadyPanel.Visibility = Visibility.Hidden;
            ResultGrid.Visibility = Visibility.Visible;
            ResultList.ItemsSource = Instances.decks.PlayerDecks[_player - 1];
        }

        private void Ready_Click(object sender, RoutedEventArgs e)
        {
            DisplayResultGrid();
        }

        private void Done_Click(object sender, RoutedEventArgs e)
        {
            _player++;
            if (_player <= _numberOfPlayers)
                DisplayReadyPanel();
            else
                Instances.mainWindow.MainWindowFrame.Content = Instances.mainMenuPage;
        }
    }
}
