﻿using DraftWPF.Entities;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace DraftWPF.Pages
{
    public partial class CardListPage : Page
    {
        private Card _card;
        private ObservableCollection<Card> _allCards;

        public CardListPage(Side side, List<Set> sets)
        {
            InitializeComponent();
            _allCards = new ObservableCollection<Card>(Instances.cardPool.allCards.Where(c => c.CardSide == side && sets.Contains(c.CardSet)));
            CardList.ItemsSource = _allCards;
        }

        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            _card = (sender as Button).DataContext as Card;
            if (_card.NumbersUsed < _card.MaxNumbersUsed)
            {   
                _card.NumbersUsed++;
                CardList.Items.Refresh();
            }
        }

        private void RemoveButton_OnClick(object sender, RoutedEventArgs e)
        {
            _card = (sender as Button).DataContext as Card;
            if (_card.NumbersUsed > 0)
            {   
                _card.NumbersUsed--;
                CardList.Items.Refresh();
            }
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            Instances.mainWindow.MainWindowFrame.Content = Instances.mainMenuPage;
        }

        private void RemoveAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (Card card in _allCards)
            {
                card.NumbersUsed = 0;
            }
            CardList.Items.Refresh();
        }

        private void AddAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (Card card in _allCards)
            {
                card.NumbersUsed = 3;
            }
            CardList.Items.Refresh();

        }
    }
}
