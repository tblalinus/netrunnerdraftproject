﻿using DraftWPF.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DraftWPF.Pages
{

    public partial class CardListMenuPage : Page
    {

        private List<Set> _setsSelected;
        private Side? _side = null;

        public CardListMenuPage()
        {
            InitializeComponent();
            _setsSelected = new List<Set>();
        }

        private void RunnerButton_Checked(object sender, RoutedEventArgs e)
        {
            _side = Side.Runner;
        }

        private void CorpButton_Checked(object sender, RoutedEventArgs e)
        {
            _side = Side.Corp;
        }

        private void CoreBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Core);
        }

        private void CoreBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Core);
        }

        private void GenesisBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Genesis);
        }

        private void GenesisBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Genesis);
        }

        private void CaCBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.CaC);
        }

        private void CaCBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.CaC);
        }

        private void SpinBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Spin);
        }

        private void SpinBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Spin);
        }

        private void HaPBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.HaP);
        }

        private void HaPBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.HaP);
        }

        private void LunarBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Lunar);
        }

        private void LunarBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Lunar);
        }

        private void OaCBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.OaC);
        }

        private void OaCBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.OaC);
        }

        private void SanSanBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.SanSan);
        }

        private void SanSanBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.SanSan);
        }

        private void DaDBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.DaD);
        }

        private void DaDBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.DaD);
        }

        private void MumbadBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Mumbad);
        }

        private void MumbadBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Mumbad);
        }

        private void FlashpointBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.Flashpoint);
        }

        private void FlashpointBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.Flashpoint);
        }

        private void TDBox_Checked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Add(Set.TD);
        }

        private void TDBox_Unchecked(object sender, RoutedEventArgs e)
        {
            _setsSelected.Remove(Set.TD);
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (_side.HasValue && _setsSelected.Count > 0)
            {
                Instances.mainWindow.MainWindowFrame.Content = new CardListPage((Side)_side, _setsSelected);
            }
        }
    }
}
