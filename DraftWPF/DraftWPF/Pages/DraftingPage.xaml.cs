﻿using System.Windows;
using System.Windows.Controls;
using DraftWPF.Entities;

namespace DraftWPF.Pages
{
    public partial class DraftingPage : Page
    {
        private int _numberOfPlayers;
        private int _pack;
        private int _activePlayer;
        private int _handNumber;
        private int _round;
        private Decks _decks = Instances.decks;

        public DraftingPage(int numberOfPlayers)
        {
            InitializeComponent();
            _numberOfPlayers = numberOfPlayers;
            _pack = 1;
            Setup();
        }

        private void Setup()
        {
            _activePlayer = 1;
            _handNumber = 1;
            _round = 1;
            if (_pack <= 4)
                DisplayReadyPanel();
            else
                Instances.gamePage.GameFrame.Content = new ResultPage(_numberOfPlayers);
        }

        private void Ready_OnClick(object sender, RoutedEventArgs e)
        {
            DisplaySelectionGrid();
        }

        private void DisplayReadyPanel()
        {
            SelectionGrid.Visibility = Visibility.Hidden;
            ReadyPanel.Visibility = Visibility.Visible;
            PlayerTurnText.DataContext = $"PLAYER {_activePlayer}'s TURN";
        }

        private void DisplaySelectionGrid()
        {
            SetSources();
            ReadyPanel.Visibility = Visibility.Hidden;
            SelectionGrid.Visibility = Visibility.Visible;
            Select.IsEnabled = false;
        }

        private void SetSources()
        {
            PlayerSelectionText.DataContext = $"Player {_activePlayer}'s selected cards";
            HandList.ItemsSource = _decks.Hands[_handNumber-1];
            SelectedList.ItemsSource = _decks.PlayerSelections[_activePlayer - 1];
        }

        private void HandList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedList.UnselectAll();
            CardPreview.DataContext = HandList.SelectedItem;
            Select.IsEnabled = true;
        }

        private void HandList_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            SelectedList.UnselectAll();
            CardPreview.DataContext = HandList.SelectedItem;
            Select.IsEnabled = true;
        }

        private void SelectedList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HandList.UnselectAll();
            CardPreview.DataContext = SelectedList.SelectedItem;
            Select.IsEnabled = false;
        }

        private void SelectedList_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            HandList.UnselectAll();
            CardPreview.DataContext = SelectedList.SelectedItem;
            Select.IsEnabled = false;
        }

        private void Select_Click(object sender, RoutedEventArgs e)
        {
            Card selectedCard = (Card)HandList.SelectedItem;
            _decks.SelectCard(_activePlayer, _handNumber, selectedCard);
            CompleteRound();
        }

        private void CompleteRound()
        {
            GetNextPlayer();

            if (_round <= 10)
                DisplayReadyPanel();
            else
            {
                _decks.CreateDecks();
                _pack++;
                Setup();
            }
        }

        private void GetNextPlayer()
        {
            _activePlayer++;
            GetHandNumber();

            if (_activePlayer > _numberOfPlayers)
            {
                _activePlayer = 1;
                GetHandNumber();
                _round++;   
            }
        }

        private void GetHandNumber()
        {
            _handNumber++;

            if (_handNumber > _numberOfPlayers)
                _handNumber = 1;
        }
    }
}
