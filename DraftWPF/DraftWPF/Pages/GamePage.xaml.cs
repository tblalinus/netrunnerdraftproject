﻿using DraftWPF.Entities;
using System.Windows;
using System.Windows.Controls;

namespace DraftWPF.Pages
{
    public partial class GamePage : Page
    {
        public GamePage()
        {
            InitializeComponent();
            GameFrame.Content= new GameMenuPage();
        }

        private void Menu_OnClick(object sender, RoutedEventArgs e)
        {
            Instances.mainWindow.MainWindowFrame.Content = Instances.mainMenuPage;
        }
    }
}
