﻿using System.Windows;
using DraftWPF.Pages;
using DraftWPF.Entities;

namespace DraftWPF
{
    public enum Side
    {
        Runner, Corp
    }

    public enum Set
    {
        Core, Genesis, CaC, Spin, HaP, Lunar, OaC, SanSan, DaD, Mumbad, Flashpoint, RedSand, TD
    }

    public partial class MainWindow
    {
        public MainWindow()
        {
            Instances.mainWindow = this;
            Instances.mainMenuPage = new MainMenuPage();
            InitializeComponent();
            WindowState = WindowState.Maximized;
            WindowStyle = WindowStyle.None;
            MainWindowFrame.Content = Instances.mainMenuPage;
        }
    }
}
