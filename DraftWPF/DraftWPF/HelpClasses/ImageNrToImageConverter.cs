﻿using DraftWPF.Entities;
using System;
using System.Globalization;
using System.Windows.Data;

namespace DraftWPF.HelpClasses
{
    class ImageNrToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value.GetType() != typeof(Card))
                return null;

            Card card = (Card)value;

            return $"/Resources/{card.CardSide}Images/{card.CardSet}/{card.CardNr}.jpg";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
