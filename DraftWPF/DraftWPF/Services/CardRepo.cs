﻿using DraftWPF.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DraftWPF.Services
{
    public class CardRepo
    {
        private readonly string _path;

        private CardRepo()
        {
            _path = AppDomain.CurrentDomain.BaseDirectory;
            _path = Path.Combine(_path, "Resources");
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);
            _path = Path.Combine(_path, "CardList.xml");
        }

        public void Save() { }

        public List<Card> Load()
        {
            List<Card> Cards = new List<Card>();
            return Cards;
        }
    }
}
