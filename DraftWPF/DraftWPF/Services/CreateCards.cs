﻿using DraftWPF.Entities;
using System.Collections.Generic;

namespace DraftWPF.Services
{
    public static class CreateCards
    {
        private static List<Card> CardList = new List<Card>();

        public static List<Card> createCards()
        {
            AddSet(Set.Core, 56, 50);
            AddSet(Set.Genesis, 60, 53);
            AddSet(Set.CaC, 24, 25);
            AddSet(Set.Spin, 65, 53);
            AddSet(Set.HaP, 24, 25);
            AddSet(Set.Lunar, 67, 46);
            AddSet(Set.OaC, 24, 25);
            AddSet(Set.SanSan, 62, 51);
            AddSet(Set.DaD, 25, 24);
            AddSet(Set.Mumbad, 62, 48);
            AddSet(Set.Flashpoint, 62, 50);
            AddSet(Set.TD, 28, 25);

            return CardList;
        }

        private static void AddSet (Set set, int corpNumber, int runnerNumber)
        {
            for (int i = 0; i < runnerNumber; i++)
            {
                Card card = new Card(Side.Runner, set, i + 1, 3);
                CardList.Add(card);
            }

            for (int i = 0; i < corpNumber; i++)
            {
                Card card = new Card(Side.Corp, set, i + 1, 3);
                CardList.Add(card);
            }
        }
    }
}
